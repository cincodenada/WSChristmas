#!/usr/bin/env python
# vim: ts=2 sts=2 sw=2

import asyncio
import websockets
import json
import sys
from datetime import datetime

from tree import TreeSerial
from translate import Serialize

wsaddr = "wss://better-magic-smartphone.glitch.me/ws"

class TreeController:
	def __init__(self, ws):
		self.ws = ws

	def cmd(self, type, data):
		return self.ws.send(json.dumps({
			"type": type,
			"data": data
		}))

async def go(tree):
	async with websockets.connect(wsaddr) as websocket:
		lights = TreeController(websocket)
		await lights.cmd('id', 'cincodenada')
		await lights.cmd('halves', tree.halves)
		await lights.cmd('num_lights', tree.num_lights)
		while True:
			msg = await websocket.recv()
			print(datetime.now())
			print(msg)
			command = json.loads(msg)
			colors = []
			for byteArray in Serialize.from_json(command):
				tree.write(byteArray)

			if command['command'] == 'get_colors':
				lines = tree.read().split("\r\n")
				await lights.cmd('colors', lines[0])
				print("\r\n".join(lines[1:]))

			print(tree.read())
			sys.stdout.flush()

tree = TreeSerial(sys.argv[1])
(size, halves) = tree.read_size()
if(len(halves) <= 1):
	print(tree.read())
	for byteArray in Serialize.from_json({
		"command": "set_halves",
		"halves": [20, 37, 57, 74, 94, 111, 129, 143, 159, 172, 186, 195, 208, 215, 225, 231, 238, 241, 244]
	}):
		tree.write(byteArray)
	
	print(tree.read())

while True:
	try:
		asyncio.run(go(tree))
	except KeyboardInterrupt as e:
		break
	except Exception as e:
		print(f"Error running tree: {e}")
		print("Reconnecting...")
		tree.reconnect()
