import sys
import math
import numpy as np
from operator import mul
from PIL import Image, ImageShow

from struct import pack
import threading
from playsound import playsound

from tree import TreeSerial
from audio_fft import AudioFft, AudioFileSource, MicSource

class SonicTree:
  def __init__(self, lights, framelen, colormap, skip=0):
    self.lights = lights
    self.skip = skip
    self.bincount = lights - skip
    self.max = 255
    self.fft = AudioFft(framelen)
    self.colormap = colormap

  # We get window_samp/2-1 useful frequency bins
  # Ideally we want 250 bins for 250 lights
  # So, window_samp should be at least 512
  # But we also want to chunk up the audio at a framerate
  # we can push, which is about 5fps or so given our data
  # rate, which is 4096
  # So we'll just add the close-to-each-other bins
  # together, maybe with some frequency overlap as well
  # for fun?
  # 4096/512 = 8, so we want to take 8 bins per bin...

  def get_scaled(self, source, log=True, **kwargs):
    for bins in self.fft.get_logbins(source, self.bincount, **kwargs):
      bins = self.scale_bins(bins)
      yield np.array([np.array(mapRGB(v)) for v in bins])
      
  def scale_bins(self, bins, log = True):
    self.max = max(self.max, np.max(bins))
    arr = np.array([0]*self.skip + bins)
    if log:
      div = math.log10(self.max)/255
      return np.uint8(np.log10(arr+1)/div)
    else:
      div = self.max/255
      return np.uint8(arr/div)

def mapWhite(val):
  return [val, val, val]

def mapRGB(val):
  b = max(0, (128-val)*2-1)
  g = val*2 if val < 128 else (255 - val)*2
  r = val*2 if val < 128 else 255
  return [r, g, b]

#print([mapRGB(i) for i in range(256)])

sonic = SonicTree(250, 1/15, mapRGB)
if sys.argv[1][0] == '/':
  tree = TreeSerial(sys.argv[1])
  tree.read_size()

  if len(sys.argv) > 2:
    source = AudioFileSource(sys.argv[2])
    threading.Thread(target=playsound, args=(sys.argv[2],)).start()
  else:
    source = MicSource()

  for colors in sonic.get_scaled(source):
    tree.write([251, len(colors)] + TreeSerial.pack([b >> 4 for b in colors.flatten()]) + [0xFF]*10)
else:
  source = AudioFileSource(sys.argv[1])
  scaled = np.uint8([
    np.uint8(l) for l in sonic.get_scaled(source, steady=False)
  ])
  img = Image.fromarray(scaled, mode="RGB")
  img.show()

