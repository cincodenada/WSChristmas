#define PINS_ESP8266

#ifdef PINS_ESP8266
#define PIN_UP 12
#define PIN_DOWN 4
#define PIN_ENTER 14
#define PIN_DIAL A0
#define PIN_DATA 5
#endif

#ifdef PINS_TEENSY4
#define PIN_UP 4
#define PIN_DOWN 9
#define PIN_ENTER 11
#define PIN_DIAL A6
#define PIN_DATA 10
#endif
