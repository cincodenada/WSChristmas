import math
import numpy as np
from scipy.fftpack import fft, ifft
import librosa
from time import sleep
from datetime import datetime

import pyaudio

class LogBins:
  def __init__(self, minbin, maxbin, outbins):
    self.minbin = minbin
    self.maxbin = maxbin
    self.outbins = outbins
    self.binmap = self.calc_bins()

  def calc_bins(self):
    inbins = self.maxbin - self.minbin
    maxval = math.log(inbins)
    last_bin = 0
    contained_bins = []
    binmap = []
    for inbin in range(self.minbin, self.maxbin):
      target_bin = math.floor(math.log(inbin-self.minbin+1)/maxval*self.outbins)
      if(target_bin > last_bin):
        for i in range(last_bin, target_bin):
          binmap.append(contained_bins)
        contained_bins = []
        last_bin = target_bin
      contained_bins.append(inbin)
    if len(contained_bins):
      binmap.append(contained_bins)
    return binmap

  def map_bins(self, inbins):
    return [
      np.mean([inbins[idx] for idx in binidxs])
      for binidxs in self.binmap
    ]

  def get_notes(self, binwidth):
    meanbins = [
      sum(bins)/len(bins) if len(bins) else 0
      for bins in self.binmap
    ]
    return [
      librosa.hz_to_note(binidx*binwidth)
      for binidx in meanbins
    ]

def windowsum(data, width, overlap):
  window = np.hamming(width*(1+overlap*2))
  return reduce(
    lambda a: sum(map(mul, a, window)),
    data, width, overlap
  )

def reduce(fun, data, width, overlap):
  margin = math.floor(width*overlap)
  return [
    fun(data[i-margin:i+margin])
    for i in range(margin,len(data)-margin,width)
  ]

def steadychunks(chunks, chunk_time):
  total_sleep = 0
  sleeps = 0
  for chunk in chunks:
    before = datetime.now().timestamp()
    yield chunk
    after = datetime.now().timestamp()
    nextframe = before + chunk_time
    if after < nextframe:
      delay = nextframe - after
      total_sleep += delay
      sleeps+=1
      sleep(delay)
  print(f"Slept {sleeps}/{len(chunks)} for a total of {total_sleep}")

class AudioFileSource:
  def __init__(self, filename):
    print(f"Loading audio...")
    wavdata, fs = librosa.load(filename, sr=None)
    self.fs = fs
    self.data = wavdata
    self.filename = filename

  def get_chunks(self, chunksize):
    return np.array_split(self.data, len(self.data)//chunksize)

class MicSource:
  def __init__(self, fs=22050):
    self.fs = fs

  def get_chunks(self, chunksize):
    p = pyaudio.PyAudio()
    stream = p.open(
      format=pyaudio.paUInt8,
      channels=1,
      rate=self.fs,
      input=True,
      output=False,
      frames_per_buffer=chunksize,
      input_device_index=self.get_pulse(p)
    )

    try:
      while True:
        yield np.fromstring(stream.read(chunksize), np.uint8)
    except Exception as e:
      print(e)
      pass
    
    stream.close()
    p.terminate()

  def get_pulse(self, pa):
    for i in range(pa.get_device_count()):
      info = pa.get_device_info_by_index(i)
      if(info["name"] == "pulse"):
        return i

class AudioFft:
  def __init__(self, target_length):
    self.target_length = target_length

  def window_size(self, fs):
    return 2**math.floor(math.log2(fs*self.target_length))

  def get_binses(self, source, overlap=0.5, steady=True):
    window_samp = self.window_size(source.fs)
    chunksize = math.floor(window_samp/(1+overlap))
    chunktime = chunksize/source.fs
    offset = window_samp - chunksize

    print(f"Analyzing with chunk size of {window_samp}/{chunksize} samples ({chunktime*1000:0.0f} ms), overlap {overlap:0.0%}...")

    chunks = source.get_chunks(chunksize)
    prev_chunk = np.full(window_samp, 0, dtype=np.uint8)

    if steady:
      chunks = steadychunks(chunks, chunktime)
    for chunkdata in chunks:
      fullchunk = np.concatenate((prev_chunk[chunksize+1:], chunkdata))
      yield np.abs(fft(fullchunk))
      prev_chunk = fullchunk[-window_samp:]

  def get_logbins(self, source, outbins, lowpass = 100, **kwargs):
    numbins = self.window_size(source.fs)
    binwidth = source.fs/numbins
    # +1 cause we always want to skip the 0 bin
    # This math might be wrong (are bins centered??) but it's close enough
    minbin = max(math.floor(lowpass/binwidth), 1)
    # Cheating a bit here, this knowledge is shared between here and get_magses
    binmap = LogBins(minbin, numbins//2, outbins)
    clist = {note: idx for idx, note in enumerate(binmap.get_notes(binwidth)) if len(note) == 2 and note[0] == 'C'}
    print(clist)
    for bins in self.get_binses(source, **kwargs):
      yield binmap.map_bins(bins)
