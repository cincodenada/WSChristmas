#!/usr/bin/env python
# vim: ts=2 sts=2 sw=2

import serial

class TreeSerial:
  def __init__(self, seraddr='/dev/tree'):
    self.log = open("treecmd.log", "a")
    self.ser = serial.Serial(seraddr, 115200)
    self.ser.timeout = 0.5
    self.drain()

  def reconnect(self):
    try:
      self.ser.close()
      self.ser.open()
    except Exception as e:
      print(f"Reconnect failed: {e}")

  def read_size(self):
    self.write([254, 127]) # XOP STATE_TREE
    params = self.drain()

    if not params:
      # Try putting it in command mode
      self.write(b'\ncmd\n')
      self.write([254, 127]) # XOP STATE_TREE
      params = self.drain()

    params = params.decode('ascii').split("\n")

    try:
      info = []
      # Null byte indicates end of state
      while params[0][0] != '\0':
        info.append(int(params.pop(0)))
    except IndexError:
      # We just ran out of things, that's fine
      pass

    print(info)
    print("\n".join(params)[1:])

    self.num_lights = info[0]
    self.halves = info[1:]

    return (self.num_lights, self.halves)

  def drain(self):
    response = b""
    while True:
      result = self.ser.read(100)
      if not result:
        return response
      response += result

  def read(self):
    return self.drain().decode('ascii')

  def write(self, ints):
    try:
      logline=' '.join([str(i) for i in ints])
      self.log.write(logline + "\n")
      self.log.flush()
    except Exception as e:
      print(f"Failed to log: {e}")

    try:
      return self.ser.write(bytes(ints))
    except Exception as e:
      print("Failed to write:")
      print(ints)
      raise e
