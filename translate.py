class Serialize:
  def from_json(command):
    cmd = command['command']
    if cmd == 'set_tree':
        colors = command['colors']
        colorBytes = [b for h in colors for b in Serialize.getColorBytes(h, 4)]
        packedBytes = Serialize.pack(colorBytes)
        yield [251, len(colors)] + packedBytes + [0xFF]*10

    elif cmd == 'set_color' or cmd == 'set_colors':
        colors = [command] if cmd == 'set_color' else command['colors']

        for setcolor in colors:
            if setcolor["color"] is None or setcolor["idx"] is None:
                continue
            color = Serialize.getColorBytes(setcolor["color"])
            yield [int(setcolor["idx"])] + color
    elif cmd == 'set_anim':
        if('mode' in command):
            yield [254, 129, 254, int(command['mode'])]
        if('fps' in command):
            yield [254, 129, 255, int(command['fps'])]
    elif cmd == 'get_colors':
        yield [254, 4]
    elif cmd == 'set_halves':
        yield [254, 130, len(command['halves'])] + command['halves']

  def getColorBytes(hexcolor, bitspercolor=8):
      if not hexcolor:
          hexcolor = '#000'
      if hexcolor[0] == '#':
          hexcolor = hexcolor[1:]

      shift = 8 - bitspercolor
      if len(hexcolor) == 3:
          triplet = [c*2 for c in hexcolor]
      else:
          triplet = [hexcolor[i:i+2] for i in range(0, len(hexcolor), 2)]
      return [int(c, 16) >> shift for c in triplet]

  def pack(colorBytes):
      numColors = len(colorBytes)
      return [
          (colorBytes[i] << 4) + colorBytes[i+1] if i+1 < numColors else 0
          for i in range(0, numColors, 2)
      ]

