# Winternet Lights

This is the code behind the lights on the internet-connected tree in my living
room. It's been an ongoing project since December 2021, and has evolved in many
ways over the years.

This is the hardware half of the project. Anyone can go to the companion site,
currently hosted at [https://tree.c9a.dev](https://tree.c9a.dev), and draw
colors on the tree or choose animation options.

## What is this?

It serves as a small project bringing random strangers together to interact over
the focused medium of a few hundred lights on a tree in some other stranger's
living room - whether they happened by my living room window, or saw a post
about it that wound its way from my social web to theirs.

Over the years a contingent of folks from around the globe have stopped in to
twiddle the lights, leaving artistic designs, pride flag colors, random
scribbles. Kids from halfway across the world would log on after school to chat
with each other and change the lights together.

This project owes inspiration to the Panic Sign, a light-up logo at the top of a
building a few blocks up the street from Powell's, the offices of Panic Software
(who brought you a really good software for MacOS, and eventually Firewatch and
the untitled goose game).

The sign debuted in Portland about the same time that I moved here, and I've
enjoyed its existence for over a decade. It had two halves, and anyone who knew
about it could go to [sign.panic.com][https://sign.panic.com] and choose what
colors each half was lit up with. It was always a little joy to pull out your
phone (or tell a friend to), push a couple buttons, and look up and see your
actions affect a sign way a few blocks down, at the top of a multi-story
building. And you got to express your little bit of creativity and aesthetic
decision for all to see - at least, until someone else who was in the know
picked a different scheme.

I also took inspiration from other much more elaborate collaborative internet
art pieces, such as [One Million
Checkboxes](https://en.wikipedia.org/wiki/One_Million_Checkboxes) and Reddit's
[/r/Place](https://en.wikipedia.org/wiki/R/place).

## Technical Overview

As of the 2024 instance, it runs entirely on a microcontroller! While previous
years it required a computer to do the internet connectivity and relay
instructions via USB to an attached microcontroller, this year I migrated all
the code to an ESP8266, which when booted up finds my wifi and connects to the
web application to wait for commands.

The ESP8266 uses Arudino and runs a very bespoke light controller that I've
cobbled together over the past few years that receives commands from the
internet via a websocket connection, and drives a string of 250 WS2812 LEDs. The
controller handles updating the colors and a handful of animation options, as
well as setup, most notably mapping the lights to their physical layout on the
tree.

This is accomplished in a relatively simple fashion: the controller has three
buttons connected to it, and when the Action button is pressed, it goes into
configuration mode. In this mode, I can select individual lights with the Up and
Down buttons, and by doing so, mark a vertical line of LEDs up one side of the
tree and down the other. This divides the lights up into a number of sections,
each of which corresponds to one half-turn around the tree.

The controller then reports these section locations to the webapp, which renders
a spiral using the known position of the marked LEDs, spreading the rest of the
LEDs evenly within their sections. This acheives a very reasonable spatial
mapping of the tree with a very simple and flexible setup process.

### Components

I currently use the [Huzzah ESP8266 Breakout from Adafruit][esp8266] because
that's what I had laying around in a drawer. If you're looking for
recommendations, I would recommend going with the similar [Feather
Huzzah][feather], which is essentially the same thing but has an integrated USB
port, which is worth it in my opinion - I spent way too long digging around for
an FTDI serial programmer for my board. And if you want more power, definitely
check out an [ESP32-based Feather][esp32] - they don't have quite the support or
documentation of the 8266, but they are the latest and greatest as far as I/O
and processing speed goes.

On the web side, I host the control app on Glitch, which is an excellent
platform for little projects like this. You can [look over and fork the
code][glitchsite] over there if you like. The basic architecture is that it
listens for websocket connections (with an authorization secret) from the tree
controller, receives information about the tree layout from the controller, and
uses that to render a tree that aligns with the acutal layout of the LEDs.

Then when people draw on the tree, the website uses that socket to send commands
back to the tree to update the colors and animations.

The site also embeds a livestream that is running via [Owncast][owncast] - an
open-source, self-hosted streaming server that is an alternative to proprietary
services like Twitch. The tree is streamed via two cameras, one of which is an
old Reolink camera that has been leant new life thanks to the [neolink
project][neolink]

[esp8266]: https://www.adafruit.com/product/2471
[feather]: https://www.adafruit.com/product/2821
[esp32]: https://www.adafruit.com/product/3405
[glitchsite]: https://glitch.com/edit/#!/better-magic-smartphone
[owncast]: https://owncast.online
[neolink]: https://github.com/QuantumEntangledAndy/neolink

### Details

This repository is now split into two: the `main` branch contains the Arduino
code for the microcontroller, and the `library` branch is an Arudino library
that contains some core common code, as well as some testing applications to
test the library code without an actual tree on hand, which makes it much easier
to debug things.
