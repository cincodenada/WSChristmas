#TARGET=teensy:avr:teensy40
#TARGET=teensy:avr:teensyLC
TARGET=esp8266:esp8266:huzzah
ARDUINO_BASE=/home/ell/Arduino/libraries

verify:
	arduino --verify WinternetLights.ino --board $(TARGET) --preserve-temp-files --pref build.path="build/"

upload:
	arduino --upload WinternetLights.ino --port /dev/ttyUSB0 --board $(TARGET) --pref upload.speed=115200 --pref cache.enable=true --pref compiler.cache_core=true

db:
	arduino-cli compile --only-compilation-database --build-path ./build -b $(TARGET)

start:
	. .venv/bin/activate
	python websocket.py
