#include "pins.h"

#include "TreeLights.h"
#include "websocket.h"

const int numled = 250;

// Usable pins:
//   Teensy LC:   1, 4, 5, 24
//   Teensy 3.2:  1, 5, 8, 10, 31   (overclock to 120 MHz for pin 8)
//   Teensy 3.5:  1, 5, 8, 10, 26, 32, 33, 48
//   Teensy 3.6:  1, 5, 8, 10, 26, 32, 33
//   Teensy 4.0:  1, 8, 14, 17, 20, 24, 29, 39

AdafruitInterface<numled> controller(PIN_DATA);
TreeLights<numled, AdafruitInterface<numled>> leds(controller);

//char halves[] = {20, 37, 57, 74, 94, 111, 129, 143, 159, 172, 186, 195, 208, 215, 225, 231, 238, 241, 244};
//HardcodedMemory<numled> configMem(halves, sizeof(halves));
EepromMemory<numled> configMem;

uint32_t rainbow[6] = {RED, ORANGE, YELLOW, GREEN, BLUE, PURPLE};

int animStep = 0;
int animStepStep = 1;
int animMax = 100;
unsigned long nextUpdate = 0;
int fps = 10;
bool animReverse = true;

int curEffect = 0;

double brightness;

TreeSocket client{numled};
CommandMode<decltype(leds), numled> config{leds, treeRpc, configMem};

void setup() {
  controller.begin();
  Serial.begin(115200);

  pinMode(PIN_UP, INPUT_PULLUP);
  pinMode(PIN_DOWN, INPUT_PULLUP);
  pinMode(PIN_ENTER, INPUT_PULLUP);
  pinMode(PIN_DIAL, INPUT);

  controller.setBrightness(analogRead(PIN_DIAL)>>2);

  client.setHalves(leds.sections.sectionPos, leds.sections.numSections);
  client.setup();

}

void loop() {
  // change all the LEDs in 1.5 seconds
  // int microsec = 1/10*1e6 / controller.numPixels();

  Command cmd = config.readCmd();
  if (config.isInternal(cmd)) {
    config.run(cmd);
  } else {
    switch (cmd) {
      case Command::LEFT:
      case Command::UP:
        curEffect++;
        break;
      case Command::RIGHT:
      case Command::DOWN:
        curEffect--;
        break;
      default:
        break;
    }
    handleRun();
  }

  leds.showAnim();

  client.poll();
}

void doEffect() {
  int checkEffect = 0;
  auto thisOne = [&checkEffect] {
    if (micros() % 100 == 0) {
      /*
      Serial.write(curEffect);
      Serial.write("<->");
      Serial.println(checkEffect);
      */
    }
    if (curEffect == checkEffect) {
      return true;
    } else {
      checkEffect++;
      return false;
    }
  };

  if (!((thisOne() && animRainbow())                //
        || (thisOne() && animSequentialSections())  //
        || (thisOne() && animOopsButCool())         //
        || (thisOne() && animFade(PURPLE))          //
        || (thisOne() && colorSequence(2, BLUE, BLUE, BLUEPURPLE, PINK, PINK,
                                       BLUEPURPLE))                  //
        || (thisOne() && colorSequence(3, BLUE, PINK, WHITE, PINK))  //
        || (thisOne() && colorSequence(3, PINK, YELLOW, BLUE, OFF))  //
        )) {
    if (curEffect == -1) {
      curEffect = checkEffect - 1;
    } else {
      curEffect = 0;
    }
    doEffect();
  }
}

void handleRun() {
  unsigned long curTime = micros();
  if (curTime < nextUpdate) {
    return;
  }

  doEffect();

  nextUpdate = curTime + 1e6 / fps;

  animStep += animStepStep;
  if (animStep > animMax) {
    if (animReverse) {
      animStepStep = -1;
    } else {
      animStep = 0;
    }
  } else if (animStep <= 0) {
    if (animReverse) {
      animStepStep = 1;
    } else {
      animStep = animMax;
    }
  }
}

bool animSequentialSections() {
  int curSection = 0;
  for (int i = 0; i < numled; i++) {
    if (curSection == animStep) {
      leds.setPixel(i, PURPLE);
    } else {
      leds.setPixel(i, OFF);
    }
    if (leds.sections.isDivider(i)) {
      ++curSection;
    }
  }
  return true;
}

bool animOopsButCool() {
  int curColorIdx = 0;
  int numColors = sizeof(rainbow) / sizeof(int);
  for (int i = 0; i < numled; i++) {
    leds.setPixel(i, (rainbow[curColorIdx] + animStep) % numColors);
    if (leds.sections.isDivider(i)) {
      ++curColorIdx;
    }
  }
  animMax = numColors;
  return true;
}

bool animRainbow() {
  int curColorIdx = 0;
  int numColors = sizeof(rainbow) / sizeof(int);
  for (int i = 0; i < numled; i++) {
    leds.setPixel(i, rainbow[(curColorIdx + animStep) % numColors]);
    if (leds.sections.isDivider(i)) {
      ++curColorIdx;
    }
  }
  animMax = numColors;
  return true;
}
bool animFade(uint32_t color) {
  float fadeValue = animStep / (float)animMax;
  uint32_t fadeColor = 0;
  for (int j = 0; j < 3; j++) {
    fadeColor += (uint32_t)(((color >> j * 8) & 0xFF) * fadeValue) << j * 8;
  }
  for (int i = 0; i < controller.numPixels(); i++) {
    leds.setPixel(i, fadeColor);
  }
  return true;
}
template <typename... Args>
bool colorSequence(size_t repeat, Args... colorArgs) {
  int idx = 0;
  uint32_t colors[] = {(uint32_t)colorArgs...};
  while (true) {
    for (auto color : colors) {
      for (size_t i = 0; i < repeat; i++) {
        leds.setPixel(idx, color);
        idx++;
        if (idx >= numled) {
          return true;
        }
      }
    }
  }
}

bool colorRun(uint32_t color, int wait) {
  for (int i = 0; i < controller.numPixels(); i++) {
    controller.setPixelColor(i, color);
    controller.setPixelColor((i + controller.numPixels() - 1) % controller.numPixels(), OFF);
    controller.show();
    delayMicroseconds(wait);
  }
  return true;
}

bool colorWipe(uint32_t color, int wait) {
  for (int i = 0; i < controller.numPixels(); i++) {
    controller.setPixelColor(i, color);
    controller.show();
    delayMicroseconds(wait);
  }
  return true;
}

bool colorDebug() {
  const int count = controller.numPixels();
  while (true) {
    for (uint32_t i = 0;; i++) {
      controller.setPixelColor(0, i, 0, 0);
      controller.setPixelColor(1, 0, i, 0);
      controller.setPixelColor(2, 0, 0, i);
      controller.setPixelColor(3, i);
      for (int b = 0; b < 32; b++) {
        controller.setPixelColor(count - 32 + b, (i & (1 << b)) ? WHITE : OFF);
      }
      controller.show();
      Serial.println(i);
    }
  }
  return true;
}
