// Replace with your network credentials
// And save this file as creds.h
const char* ssid = "YourSsidHere";
const char* password = "YourPasswordHere";
const char* websocket = "wss://your-websocket.example/ws";
const char* websocket_key = "your_key_here";
