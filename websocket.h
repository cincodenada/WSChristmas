#include <ArduinoJson.h>
#include <ArduinoWebsockets.h>
#include <ESP8266WiFi.h>
#include <deque>
#include <vector>
#include <string>
#include <cstdint>

#include "TreeLights.h"
#include "creds.h"

using namespace websockets;

void dumpMessage(String prefix, const String& str) {
    Serial.printf("%s: ", prefix);
    for(auto cp = str.begin(); cp < str.end(); cp++) {
      Serial.printf("%d ", *cp);
    }
    Serial.println();
}

class DequeStream : public SerialLike {
public:
  int read() {
    if(!available()) { return -1; }
    std::string &curVal = messages.front();
    char val = curVal.at(curIndex);
    curIndex++;
    if (curIndex >= curVal.length()) {
      messages.pop_front();
      curIndex = 0;
    }
    //Serial.printf("R: %d\n", val);
    return val;
  }
  int peek() {
    if(!available()) { return -1; }
    return messages.front().at(curIndex);
  }
  std::string readMessage() {
    std::string ret = std::move(messages.front());
    messages.pop_front();
    return ret;
  }
  bool available() { return !messages.empty(); }
  void write(std::string &&str) { messages.emplace_back(str); }
  void write(const char *str, size_t length) {
    messages.emplace_back(str, length);
  }
  void write(const char *str) {
    messages.emplace_back(str);
  }
  bool wait(size_t timeout_ms) {
    int total_delay = 0;
    int delay_inc = timeout_ms / 100;
    while (!available() && total_delay < timeout_ms) {
      delay(delay_inc);
      total_delay += delay_inc;
    }
    return available();
  }

private:
  int curIndex = 0;
  std::deque<std::string> messages;
};

class DualStream : public SerialLike {
  public:
    int read() {
      return Serial.available() ? Serial.read() : internalStream.read();
    }
    int peek() {
      return Serial.available() ? Serial.peek() : internalStream.peek();
    }

  std::string readMessage() {
    return internalStream.readMessage();
  }
  bool available() { return Serial.available() || internalStream.available(); }
  void write(std::string &&str) { internalStream.write(std::move(str)); }
  void write(const char *str, size_t count) { internalStream.write(str, count); }
  void write(const char *str) { internalStream.write(str); }
  bool wait(size_t timeout_ms) { return internalStream.wait(timeout_ms); }

  private:
  DequeStream internalStream;
};

DequeStream treeRpc;

class TreeSocket {
public:
  TreeSocket(int numLights, int heartbeatSeconds = 60) : numLights(numLights), heartbeatSeconds(heartbeatSeconds) {};

  void setHalves(const uint8_t* inhalves, size_t count) {
    halves.clear();
    for(int i=0; i < count; i++) {
      halves.push_back(inhalves[i]);
    }
  }

  void setup() {
    // Connect to wifi
    WiFi.begin(ssid, password);

    // Wait some time to connect to wifi
    for (int i = 0; i < 10 && WiFi.status() != WL_CONNECTED; i++) {
      Serial.print(".");
      delay(1000);
    }

    // Setup Callbacks
    client.onMessage([this](WebsocketsMessage message) {
      onMessageCallback(message);
    });
    client.onEvent([this](WebsocketsEvent event, String data) {
      onEventsCallback(event, data);
    });

    // Connect to server
    client.connect(websocket);

    // Send a ping
    heartbeat();
  }

  void getState() {
    treeRpc.write("\ncmd\n");
      /*
    const char getStateCmd[2] = { 254, 127 };
    treeRpc.write(getStateCmd);
    if(treeRpc.wait(1000)) {
      readState();
    } else {
      treeRpc.write("\ncmd\n");
      treeRpc.write(getStateCmd);
      if(treeRpc.wait(1000)) {
        readState();
      }
      // else???
    }
    */
  }

  void readState() {
    /*
    auto msg = treeRpc.readMessage();
    Serial.printf("READ: %s", msg.c_str());
    numLights = std::stoi(msg.c_str());
    halves.clear();
    while(treeRpc.available() && treeRpc.peek() != 0) {
      halves.push_back(std::stoi(treeRpc.readMessage().c_str()));
    }
    // Clear out
    while(treeRpc.available()) {
      treeRpc.readMessage();
    }
    */
  }
  

  void init() {
    getState();
    client.send(String("id\ncincodenada\n") + String(websocket_key));
    client.send("mode\nraw");
    String halfstr("halves");
    for (auto half = halves.begin(); half != halves.end(); half++) {
      halfstr.concat("\n");
      halfstr.concat(*half);
    }
    client.send(halfstr);
    client.send(String("num_lights\n") + String(numLights));
    Serial.println("Initialized tree");
  }

  void heartbeat() {
    // Ignore overflow; if we're running for 50 days straight we've got bigger problems
    unsigned long now = millis();
    //Serial.printf("Heartbeat: %lu/%lu (%d)\r", now, nextPing, awaitingPong);
    if(now > nextPing) {
      if(awaitingPong) {
        // will reconnect in event handler
        awaitingPong = false;
        nextPing = 0;
        Serial.println("Pong timeout, closing connection...");
        client.close();
        client.connect(websocket);
      } else {
        client.ping();
        awaitingPong = true;
        nextPing = now + heartbeatSeconds*1000;
      }
    }
  }

  void poll() {
    heartbeat();
    client.poll();
  }

private:
  void onMessageCallback(WebsocketsMessage message) {
    Serial.print("Received ");
    Serial.print(message.length());
    Serial.print(" bytes, isBinary: ");
    Serial.println(message.isBinary());
    Serial.print("Mem:");
    Serial.println(ESP.getFreeHeap());
    Serial.println(ESP.getHeapFragmentation());
    treeRpc.write(message.rawData().data(), message.length());
  }

   void onEventsCallback(WebsocketsEvent event, String data) {
    if (event == WebsocketsEvent::ConnectionOpened) {
      Serial.println("Connection Opened");
      init();
    } else if (event == WebsocketsEvent::ConnectionClosed) {
      Serial.println("Connection Closed");
      Serial.println(client.getCloseReason());
      client.connect(websocket);
    } else if (event == WebsocketsEvent::GotPing) {
      Serial.println("Got a Ping!");
    } else if (event == WebsocketsEvent::GotPong) {
      awaitingPong = false;
      Serial.println("Got a Pong!");
    }
  }

public:

private:
  WebsocketsClient client{};

  int heartbeatSeconds;
  int numLights;
  std::vector<int> halves{};

  bool awaitingPong = false;
  unsigned long nextPing = 0;
};
